<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : registerAlquiler
    Created on : 15-jul-2018, 13:03:58
    Author     : Sniwt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=windows-1256">
            <title>Alquiler Bicicletas</title>
	</head>
	<body>
            <form action="Alquiler">
                BICICLETA
                <select name="bicicletas">
                    <option>BICI1</option>
                    <option>BICI2</option>
                    <option>BICI3</option>
                    <option>BICI4</option>
                    <option>BICI5</option>
                </select>
                <c:choose>
                    <c:when test="${action.equals('create')}">
                        <input type="submit" value="Alquilar">
                    </c:when>
                    <c:when test="${action.equals('delete')}">
                        <input type="submit" value="Anular">
                    </c:when>
                </c:choose>
            </form>
	</body>
</html>