<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : alquiler
    Created on : 15-jul-2018, 11:20:58
    Author     : Sniwt
--%>
<%@page import="org.bicicletas.dao.AlquilerDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.bicicletas.entidades.Alquiler"%>
<%@ page language="java" contentType="text/html; charset=windows-1256"pageEncoding="windows-1256" import="org.bicicletas.entidades.Cliente"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=windows-1256">
       <title>Alquiler de Bicicletas </title>
    </head>
    <body>
        <form action="AlquilerOption">
            <center>
                 <% Cliente currentUser = (Cliente) (session.getAttribute("currentSessionUser"));%>
                 <%   ArrayList<Alquiler> alquileres ;
                       alquileres=AlquilerDAO.getAll(currentUser.getIdentificacion());
                       session.setAttribute("alquileres",alquileres);%>
                       
                 Welcome <%= currentUser.getNombre()+ " " + currentUser.getApellido()%>
                 <br/>
                 <table border ="1">
                         <tr>
                             <th>Identificacion</th>
                             <th>Bicicleta</th>
                             <th>Fecha Alquiler</th>
                         </tr>
                     <c:forEach items="${alquileres}" var="alquiler">
                         <tr>
                             <td>${alquiler.identificacion}</td>
                             <td>${alquiler.bicicleta}</td>
                             <td>${alquiler.alqFecha}</td>
                         </tr>
                     </c:forEach>
                 </table>
                 <br/>
                 <input type="submit" name="alquilar" value="AlquilarBicicleta">
                 <br/>
                 <input type="submit" name="anular" value="AnularAlquiler">
             </center>
        </form>
    </body>
</html>