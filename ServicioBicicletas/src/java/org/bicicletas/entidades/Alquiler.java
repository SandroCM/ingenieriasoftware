/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bicicletas.entidades;

import java.sql.Timestamp;

/**
 *
 * @author Sniwt
 */
public class Alquiler {
    private String identificacion;
    private Timestamp alqFecha;
    private String bicicleta;

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the alqFecha
     */
    public Timestamp getAlqFecha() {
        return alqFecha;
    }

    /**
     * @param alqFecha the alqFecha to set
     */
    public void setAlqFecha(Timestamp alqFecha) {
        this.alqFecha = alqFecha;
    }

    /**
     * @return the bicicleta
     */
    public String getBicicleta() {
        return bicicleta;
    }

    /**
     * @param bicicleta the bicicleta to set
     */
    public void setBicicleta(String bicicleta) {
        this.bicicleta = bicicleta;
    }
    
}
