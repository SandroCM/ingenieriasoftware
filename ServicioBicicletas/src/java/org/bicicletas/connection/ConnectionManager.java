/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bicicletas.connection;

import java.sql.*;

public class ConnectionManager {
    static Connection con;
    static String url;
    public static Connection getConnection(){
        try{
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        url = "jdbc:derby://localhost:1527/bicicletas";
            try{            	
                con = DriverManager.getConnection(url,"app","app"); 
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
        catch(ClassNotFoundException e)
        {
           System.out.println(e);
        }
      return con;
    }
}
