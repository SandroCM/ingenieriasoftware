package org.bicicletas.dao;
import java.sql.*;
import java.util.ArrayList;
import org.bicicletas.connection.ConnectionManager;
import org.bicicletas.entidades.Alquiler;

public class AlquilerDAO{
    static Connection currentCon = null;
    static ResultSet rs = null;  
   
    public static ArrayList<Alquiler> getAll(String identificacion){
        PreparedStatement pst = null;
        String searchQuery = "select * from alquiler where identificacion=?";
        ArrayList<Alquiler> alquiler = new ArrayList();
        try {
            currentCon = ConnectionManager.getConnection();
            pst=currentCon.prepareStatement(searchQuery);
            pst.setString(1, identificacion);
            rs = pst.executeQuery();
            while (rs.next()){
                Alquiler e= new Alquiler();
                e.setAlqFecha(rs.getTimestamp("fecha"));
                e.setBicicleta(rs.getString("bicicleta"));
                e.setIdentificacion(rs.getString("identificacion"));
                alquiler.add(e);
            }
        }catch (Exception ex){
            System.out.println("Alquiler.getAll failed: An Exception has occurred! " + ex);
        }finally {
            if (rs != null)	{
                try {
                   rs.close();
                } catch (SQLException e) {}
                rs = null;
            }
            if (pst != null) {
                try {
                    pst.close();
                }catch(SQLException e) {}
                pst = null;
            }
            if (currentCon != null) {
                try {
                    currentCon.close();
                }catch(SQLException e) {}
                currentCon = null;
            }
        }
    return alquiler;
   }

    public static boolean checkAlquiler(String bicicleta,String identificacion){
        PreparedStatement pst = null;
        String searchQuery = "select 1 from alquiler where identificacion=? and bicicleta = ?";
        ArrayList<Alquiler> alquiler = new ArrayList();
        try {
            currentCon = ConnectionManager.getConnection();
            pst=currentCon.prepareStatement(searchQuery);
            pst.setString(1, identificacion);
            pst.setString(2, bicicleta);
            rs = pst.executeQuery();
            boolean more = rs.next();
            if (!more) 
                return true;
        }catch (Exception ex){
            System.out.println("checkAlquiler failed: An Exception has occurred! " + ex);
        }finally {
            if (rs != null)	{
                try {
                   rs.close();
                } catch (SQLException e) {}
                rs = null;
            }
            if (pst != null) {
                try {
                    pst.close();
                }catch(SQLException e) {}
                pst = null;
            }
            if (currentCon != null) {
                try {
                    currentCon.close();
                }catch(SQLException e) {}
                currentCon = null;
            }
        }
        
        return false;
    }

    public static boolean registraAlquiler(String bicicleta,String identificacion){
        PreparedStatement pst = null;
        String searchQuery = "insert into alquiler(identificacion,bicicleta,fecha) values (?,?,?) ";
        ArrayList<Alquiler> alquiler = new ArrayList();
        try {
            currentCon = ConnectionManager.getConnection();
            pst=currentCon.prepareStatement(searchQuery);
            pst.setString(1, identificacion);
            pst.setString(2, bicicleta);
            pst.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            pst.execute();
        }catch (Exception ex){
            System.out.println("registraAlquiler failed: An Exception has occurred! " + ex);
        }finally {
            if (rs != null)	{
                try {
                   rs.close();
                } catch (SQLException e) {}
                rs = null;
            }
            if (pst != null) {
                try {
                    pst.close();
                }catch(SQLException e) {}
                pst = null;
            }
            if (currentCon != null) {
                try {
                    currentCon.close();
                }catch(SQLException e) {}
                currentCon = null;
            }
        }
        
        return true;
    }

    public static boolean anularAlquiler(String bicicleta,String identificacion){
        PreparedStatement pst = null;
        String searchQuery = "delete from alquiler where identificacion =? and bicicleta = ? ";
        ArrayList<Alquiler> alquiler = new ArrayList();
        try {
            currentCon = ConnectionManager.getConnection();
            pst=currentCon.prepareStatement(searchQuery);
            pst.setString(1, identificacion);
            pst.setString(2, bicicleta);
            pst.execute();
        }catch (Exception ex){
            System.out.println("registraAlquiler failed: An Exception has occurred! " + ex);
        }finally {
            if (rs != null)	{
                try {
                   rs.close();
                } catch (SQLException e) {}
                rs = null;
            }
            if (pst != null) {
                try {
                    pst.close();
                }catch(SQLException e) {}
                pst = null;
            }
            if (currentCon != null) {
                try {
                    currentCon.close();
                }catch(SQLException e) {}
                currentCon = null;
            }
        }
        
        return true;
    }

}