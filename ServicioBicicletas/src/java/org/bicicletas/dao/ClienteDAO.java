package org.bicicletas.dao;
import java.sql.*;
import org.bicicletas.connection.ConnectionManager;
import org.bicicletas.entidades.Cliente;

public class ClienteDAO{
    static Connection currentCon = null;
    static ResultSet rs = null;  
   
    public static Cliente login(Cliente bean) {
        PreparedStatement pst = null;        
        String username = bean.getUsuario();    
        String password = bean.getContrasenia();
        String searchQuery = "select * from cliente where usuario=? and contrasenia=?";
        try {
            currentCon = ConnectionManager.getConnection();
            pst=currentCon.prepareStatement(searchQuery);
            pst.setString(1, username);
            pst.setString(2, password);
            rs = pst.executeQuery();
            boolean more = rs.next();
            if (!more) 
            {
               bean.setValido(false);
            }else if (more) {
                String firstName = rs.getString("NOMBRE");
                String lastName = rs.getString("APELLIDO");
                String identificacion = rs.getString("IDENTIFICACION");
                bean.setNombre(firstName);
                bean.setApellido(lastName);
                bean.setIdentificacion(identificacion);
                bean.setValido(true);
            }
        }catch (Exception ex){
            System.out.println("Log In failed: An Exception has occurred! " + ex);
        }finally {
            if (rs != null)	{
                try {
                   rs.close();
                } catch (SQLException e) {}
                rs = null;
            }
            if (pst != null) {
                try {
                    pst.close();
                }catch(SQLException e) {}
                pst = null;
            }
            if (currentCon != null) {
                try {
                    currentCon.close();
                }catch(SQLException e) {}
                currentCon = null;
            }
        }
    return bean;
   }
}