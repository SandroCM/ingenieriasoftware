package org.bicicletas.handlers;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bicicletas.dao.AlquilerDAO;
import org.bicicletas.dao.ClienteDAO;
import org.bicicletas.entidades.Alquiler;
import org.bicicletas.entidades.Cliente;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) 
                                       throws ServletException, java.io.IOException {
        try{
            Cliente user = new Cliente();
            user.setUsuario(request.getParameter("un"));
            user.setContrasenia(request.getParameter("pw"));
            user = ClienteDAO.login(user);
            if (user.isValido()){
               HttpSession session = request.getSession(true);	    
               session.setAttribute("currentSessionUser",user); 
               response.sendRedirect("alquiler.jsp");
            }
            else 
                response.sendRedirect("invalidLogin.jsp");
        }catch (Throwable theException){
            System.out.println(theException); 
        }
    }
}