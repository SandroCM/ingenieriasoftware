/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bicicletas.handlers;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bicicletas.dao.AlquilerDAO;
import org.bicicletas.dao.ClienteDAO;
import org.bicicletas.entidades.Alquiler;
import org.bicicletas.entidades.Cliente;

/**
 *
 * @author Sniwt
 */
@WebServlet(name = "Alquiler", urlPatterns = {"/Alquiler"})
public class AlquilerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getSession().getAttribute("action").equals("create"))
            alquilarBicicleta(request,response);
        
        if(request.getSession().getAttribute("action").equals("delete"))
            anularBicicleta(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    private void alquilarBicicleta(HttpServletRequest request, HttpServletResponse response){
        try{
            Cliente user  = (Cliente)request.getSession().getAttribute("currentSessionUser");
            String bicicleta = request.getParameter("bicicletas");
            if (AlquilerDAO.checkAlquiler(bicicleta, user.getIdentificacion())){
                AlquilerDAO.registraAlquiler(bicicleta, user.getIdentificacion());
                response.sendRedirect("alquiler.jsp");
            }
            else 
                response.sendRedirect("invalidAlquiler.jsp");
        }catch (Throwable theException){
            System.out.println(theException); 
        }
    }
    
    private void anularBicicleta(HttpServletRequest request, HttpServletResponse response){
        try{
            Cliente user  = (Cliente)request.getSession().getAttribute("currentSessionUser");
            String bicicleta = request.getParameter("bicicletas");
            AlquilerDAO.anularAlquiler(bicicleta, user.getIdentificacion());
            response.sendRedirect("alquiler.jsp");
        }catch (Throwable theException){
            System.out.println(theException); 
        }
    }
}
